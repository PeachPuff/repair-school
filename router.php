<?php
  if (!isset($_REQUEST['length'])) {
    include 'client.html';
    die;
  }
  $length = intval($_REQUEST['length']);
  $width = intval($_REQUEST['width']);
  $tile_length = intval($_REQUEST['tile_length']);
  $tile_width = intval($_REQUEST['tile_width']);

  $command = "mix run -e 'IO.puts RepairSchool.tile_count($length,$width,$tile_length,$tile_width)'";
  error_log($command);

  $process = proc_open($command, [
      0 => ['pipe', 'r'], // STDIN
      1 => ['pipe', 'w'], // STDOUT
      2 => ['pipe', 'w']  // STDERR
  ], $pipes);

  if(is_resource($process)) {
    // If you want to write to STDIN
    //fwrite($pipes[0], '...');
    fclose($pipes[0]);

    $std_out = stream_get_contents($pipes[1]);
    fclose($pipes[1]);

    $std_err = stream_get_contents($pipes[2]);
    fclose($pipes[2]);

    $return_code = proc_close($process);

    if ($return_code!==0) {
      error_log($return_code);
      error_log($std_err);
      die(json_encode([
        'status' => 'error',
        'result' => 'Не удалось посчитать: эликсир завершился с'
                    .' ненулевым кодом возврата. Поток ошибок: '
                    .$std_err
      ]));
    }

    die(json_encode([
      'status' => 'success',
      'result' => $std_out
    ]));
  } else {
    die(json_encode([
      'status' => 'error',
      'result' => 'Не удалось посчитать: не удалось создать процесс эликсира'
    ]));
  }
?>

defmodule RepairSchool do
  def tile_count(length, width, tile_length, tile_width) do
    TileCounter.tile_count(length, width, tile_length, tile_width)
  end
end

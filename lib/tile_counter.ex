defmodule TileCounter do
  use Contracts
  @moduledoc """
  Считает количество плитки
  """

  @type length :: pos_integer
  @type width :: pos_integer

  @spec tile_count(length, width, pos_integer, pos_integer) :: pos_integer
  @spec tile_count_impl(length, width, pos_integer, pos_integer) :: pos_integer

  @doc ~S"""
  Подсчитывает количество плитки для указанного
  размера пола

  ## Примеры

      iex> TileCounter.tile_count 2, 4, 2, 1
      4

  """
  requires length>0 && width>0 and tile_length>0 and tile_width>0
  ensures result>=length*width/(tile_length*tile_width)
  def tile_count(length, width, tile_length, tile_width) do
    first_try = tile_count_impl(length, width, tile_length, tile_width)
    if tile_length==tile_width do
      first_try
    else
      second_try = tile_count_impl(length, width, tile_width, tile_length)
      if first_try<second_try do
        first_try
      else
        second_try
      end
    end
  end

  defp tile_count_impl(length, width, tile_length, tile_width)
      when rem(length, tile_length)==0
      and rem(width, tile_width)==0 do
      (length*width) |> div(tile_length*tile_width)
  end

  defp tile_count_impl(length, width, tile_length, tile_width)
      when length<=tile_length
      and width<=tile_width do
      1
  end

  defp tile_count_impl(length, width, tile_length, tile_width) do
    length_rem = rem(length, tile_length)
    width_rem = rem(width, tile_width)

    length_div = div(length, tile_length)
    width_div = div(width, tile_width)

    length_tile_count = length_div + if (length_rem != 0), do: 1, else: 0
    width_tile_count = width_div + if (width_rem != 0), do: 1, else: 0

    length_tile_count * width_tile_count
  end
end

defmodule WallpaperCounter do
  use Contracts
  @moduledoc """
  Считает количество рулонов обоев
  """
  @type length :: pos_integer
  @type height :: pos_integer
  @type width :: pos_integer
  @type door :: {height, width}
  @type window :: {height, width}
  @type wallpaper :: {height, width}

  @spec wallpaper_count(length, width, height, [door], [window], wallpaper) :: pos_integer

  requires(length>0 and width>0 and height>0
          and Enum.all?(doors, &dimensions_positive/1)
          and Enum.all?(windows, &dimensions_positive/1)
          and dimensions_positive(wallpaper))
  def wallpaper_count(length, width, height, doors, windows, {wallpaper_height, wallpaper_width} = wallpaper) do
    wall_square = (length+width)*2*height
    door_trimmed_square = doors |> Enum.filter(fn({_, door_width}) -> door_width>=width end)
                                |> Enum.map(fn({door_height, door_width}) -> door_height*door_width end)
                                |> Enum.sum()
    wall_square = wall_square - door_trimmed_square
    wallpaper_square = wallpaper_height*wallpaper_width
    
    roll_count = div(wall_square, wallpaper_square)
    if rem(wall_square, wallpaper_square)==0 do
      roll_count
    else
      roll_count+1
    end
  end

  defp dimensions_positive({height, width}) do
    height>0 and width>0
  end
end

defmodule WallpaperCounterTest do
  use ExUnit.Case
  doctest WallpaperCounter

  test "Количество рулонов обоев для самого простейшего случая верно" do
    length = 1
    width = 1
    height = 1
    doors = []
    windows = []
    wallpaper = {2, 1}

    wallpaper_count = WallpaperCounter.wallpaper_count(length, width, height,
                                                       doors, windows, wallpaper)

    assert(wallpaper_count==2)
  end

  test "Количество рулонов обоев в случае нецелого количество обоев равно" do
    length = 1
    width = 1
    height = 1
    doors = []
    windows = []
    wallpaper = {3, 1}

    wallpaper_count = WallpaperCounter.wallpaper_count(length, width, height,
                                                       doors, windows, wallpaper)

    assert(wallpaper_count==2)
  end

  test ~S"""
       Количество рулонов обоев не меняется из-за наличия двери, меньшей
       по ширине, чем ширина рулона обоев
       """ do
    length = 10
    width = 10
    height = 10
    doors = [{7, 5}]
    windows = []
    wallpaper = {20, 10}

    wallpaper_count = WallpaperCounter.wallpaper_count(length, width, height,
                                                      doors, windows, wallpaper)

    assert(wallpaper_count==2)
  end

  test "Количество рулонов обоев уменьшается, если дверь равна ширине рулона" do
    length = 1
    width = 1
    height = 1
    doors = [{1, 1}]
    windows = []
    wallpaper = {1, 1}

    wallpaper_count = WallpaperCounter.wallpaper_count(length, width, height,
                                                      doors, windows, wallpaper)

    assert(wallpaper_count==3)
  end
end

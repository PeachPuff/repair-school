defmodule TileCounterTest do
  use ExUnit.Case
  doctest TileCounter


  test "Количество плитки для пола, кратного размеру плитки, верно" do
    length = 500
    width = 400
    tile_length = 25
    tile_width = 25

    tile_count = TileCounter.tile_count(length, width, tile_length, tile_width)

    assert(tile_count==320)
  end

  test "Количество плитки верно, если одна сторона меньше размера плитки" do
    length = 20
    width = 50
    tile_length = 25
    tile_width = 25

    tile_count = TileCounter.tile_count(length, width, tile_length, tile_width)

    assert(tile_count==2)
  end

  test "Количество плитки верно, если вторая сторона меньше размера плитки" do
    length = 50
    width = 20
    tile_length = 25
    tile_width = 25

    tile_count = TileCounter.tile_count(length, width, tile_length, tile_width)

    assert(tile_count==2)
  end


  test "Количество плитки верно, если обе стороны меньше размера плитки" do
    length = 20
    width = 20
    tile_length = 25
    tile_width = 25

    tile_count = TileCounter.tile_count(length, width, tile_length, tile_width)

    assert(tile_count==1)
  end

  test "Количество плитки верно, когда одна сторона не кратна размеру плитки" do
    length = 3
    width = 2
    tile_length = 2
    tile_width = 2

    tile_count = TileCounter.tile_count(length, width, tile_length, tile_width)

    assert(tile_count==2)
  end

  test "Количество плитки верно, когда вторая сторона не кратна размеру плитки" do
    length = 2
    width = 3
    tile_length = 2
    tile_width = 2

    tile_count = TileCounter.tile_count(length, width, tile_length, tile_width)

    assert(tile_count==2)
  end

  test "Количество плитки верно, когда обе стороны не кратны размеру плитки" do
    length = 3
    width = 3
    tile_length = 2
    tile_width = 2

    tile_count = TileCounter.tile_count(length, width, tile_length, tile_width)

    assert(tile_count==4)
  end

  test "Количество плитки для прямоугольной плитки оптимально" do
    length = 3
    width = 1
    tile_length = 3
    tile_width = 1

    tile_count_first = TileCounter.tile_count(length, width, tile_length, tile_width)
    tile_count_second = TileCounter.tile_count(length, width, tile_width, tile_length)

    assert(tile_count_first==1)
    assert(tile_count_second==1)
  end

  # количество окон, площадь окон
  # объем
  # дверь
end
